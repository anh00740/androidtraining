package com.xfingers.androidtraining.widget;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

public class ExampleAppWidgetProvider extends AppWidgetProvider {
	DateFormat df = new SimpleDateFormat("hh:mm:ss");
	public static String CLOCK_WIDGET_UPDATE = "com.xfingers.androidtraining.widget.8BITCLOCK_WIDGET_UPDATE";

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		final int N = appWidgetIds.length;
		Log.i("ExampleWidget",
				"Updating widgets " + Arrays.asList(appWidgetIds));
		// Perform this loop procedure for each App Widget that belongs to this
		// provider
		for (int i = 0; i < N; i++) {
			int appWidgetId = appWidgetIds[i];
			// Create an Intent to launch ExampleActivity
			Intent intent = new Intent(context, MainActivity.class);
			PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
					intent, 0);
			// Get the layout for the App Widget and attach an on-click listener
			// to the button
			RemoteViews views = new RemoteViews(context.getPackageName(),
					R.layout.widget1);
			// views.setOnClickPendingIntent(R.id.button, pendingIntent);
			// To update a label
			// views.setTextViewText(R.id.widget1label, df.format(new Date()));
			// Tell the AppWidgetManager to perform an update on the current app
			// widget
			appWidgetManager.updateAppWidget(appWidgetId, views);
		}
	}

	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
		Log.d(this.toString(),
				"Widget Provider enabled. Starting timer to update widget every second");
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.SECOND, 5);
		alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
				5000, createClockTickIntent(context));
	}

	@Override
	public void onDisabled(Context context) {
		super.onDisabled(context);
		Log.d(this.toString(), "Widget Provider disabled. Turning off timer");
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(createClockTickIntent(context));
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		Log.d(this.toString(), "Received intent: " + intent);
		if (CLOCK_WIDGET_UPDATE.equals(intent.getAction())) {
			Log.d(this.toString(), "Clock update");
			ComponentName thisAppWidget = new ComponentName(
					context.getPackageName(), getClass().getName());
			AppWidgetManager appWidgetManager = AppWidgetManager
					.getInstance(context);
			for (int appWidgetId : appWidgetManager
					.getAppWidgetIds(thisAppWidget)) {
				updateAppWidget(context, appWidgetManager, appWidgetId);
			}
		}
	}

	private void updateAppWidget(Context context,
			AppWidgetManager appWidgetManager, int appWidgetId) {
		String currentTime = df.format(new Date());
		RemoteViews remoteView = new RemoteViews(context.getPackageName(),
				R.layout.widget1);
		remoteView.setTextViewText(R.id.txtSpelling, currentTime);
		remoteView.setTextViewText(R.id.txtPhonetics, "Phonetics "
				+ currentTime);
		remoteView.setTextViewText(R.id.txtMeaning, "Meaning " + currentTime);
		appWidgetManager.updateAppWidget(appWidgetId, remoteView);
	}

	private PendingIntent createClockTickIntent(Context context) {
		Intent intent = new Intent(CLOCK_WIDGET_UPDATE);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		return pendingIntent;
	}
}
