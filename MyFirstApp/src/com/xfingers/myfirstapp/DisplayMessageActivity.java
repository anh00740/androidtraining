package com.xfingers.myfirstapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayMessageActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // get message from the intent
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        
        // create text view
        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(message);
        
        // set text view as activity layout
        setContentView(textView);
    }

}
